package org.db.rma.lv3.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.db.rma.lv3.R
import org.db.rma.lv3.model.Task

class TaskAdapter(
	private val dataSource: List<Task>,
	val context: Context): RecyclerView.Adapter<TaskAdapter.ItemViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
		val layoutItem = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)

		return ItemViewHolder(layoutItem)
	}

	override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
		holder.textView.text = dataSource[position].text
	}

	override fun getItemCount(): Int {
		return dataSource.size
	}

	class ItemViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
		val textView = view.findViewById<TextView>(R.id.textView)
	}
}
