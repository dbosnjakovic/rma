package org.db.rma.lv3.data

import org.db.rma.lv3.model.Task

class DataSource {
	private var tasks: MutableList<Task> = mutableListOf()

	fun loadTasks(): List<Task> {
		return tasks
	}

	fun addTask(task: Task) {
		tasks.add(task)
	}
}
