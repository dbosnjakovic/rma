package org.db.rma.lv3.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import org.db.rma.lv3.R
import org.db.rma.lv3.data.DataSource
import org.db.rma.lv3.model.Task

class MainActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val dataSource = DataSource()
		var dataSet = dataSource.loadTasks()
		val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)

		findViewById<Button>(R.id.button).setOnClickListener{
			val editViewText = findViewById<EditText>(R.id.editTextToDo).text.toString();
			dataSource.addTask(Task(editViewText))
			dataSet = dataSource.loadTasks()
		}

		recyclerView.adapter = TaskAdapter(dataSet, this)
	}
}
