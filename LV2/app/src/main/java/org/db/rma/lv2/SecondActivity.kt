package org.db.rma.lv2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.Fragment

class SecondActivity : AppCompatActivity() {
	private var fragmentShown = 0

	private fun showFragment(fragment: Fragment) {
		val transaction = supportFragmentManager.beginTransaction()
		transaction.replace(R.id.fragment_main, fragment)
		transaction.commit()
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_second)

		showFragment(TextEditFragment())

		val secondActivityCloseButton: Button = findViewById(R.id.secondActivityClose)
		val changeFragmentButton: Button = findViewById(R.id.fragmentChange)

		secondActivityCloseButton.setOnClickListener {
			finish()
		}

		changeFragmentButton.setOnClickListener {
			if (fragmentShown == 1) {
				fragmentShown = 0
				showFragment(TextEditFragment())
			}
			else {
				fragmentShown = 1
				showFragment(ImageFragment())
			}
		}
	}
}
