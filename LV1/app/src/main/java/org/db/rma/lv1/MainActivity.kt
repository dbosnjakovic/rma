package org.db.rma.lv1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val button: Button = findViewById(R.id.buttonUnesi)
		button.setOnClickListener {
			val editName: TextView = findViewById(R.id.textEditIme)
			val editDescription: TextView = findViewById(R.id.textEditOpis)

			val textName: TextView = findViewById(R.id.textViewIme)
			val textDescription: TextView = findViewById(R.id.textViewOpis)

			textName.text = editName.text
			textDescription.text = editDescription.text
		}

		val bmiButton: Button = findViewById(R.id.buttonBmi)
		bmiButton.setOnClickListener {
			val editKg: TextView = findViewById(R.id.editKg)
			val editM: TextView = findViewById(R.id.editM)

			if (editM.text.isBlank() || editKg.text.isBlank()) {
				Toast.makeText(this@MainActivity, "Ispunite M i KG polja prvo!", Toast.LENGTH_SHORT).show()
			} else {
				val kg = Integer.parseInt(editKg.text.toString())
				val m = editM.text.toString().toFloat()

				Toast.makeText(this@MainActivity, (kg / (m*m)).toString(), Toast.LENGTH_LONG).show()
			}
		}
	}
}
